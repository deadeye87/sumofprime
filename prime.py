def find_prime(list1):
    sum1 = 0
    for num in list1:
        i = 2
        p = 1
        while i <= num / 2:
            if num % i == 0:
                p = 0
                break
            i = i + 1
        if p == 1:
            sum1 = sum1 + num
    return sum1


list1 = [2, 3, 4, 5, 7, 8, 9, 10]
result = find_prime(list1)
print("Sum of all prime numbers:", result)
